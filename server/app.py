from flask import Flask, jsonify, render_template, url_for, redirect, g
from flask_cors import CORS
import sql_methods_item as sqlm
import sqltojson

# configuration
DEBUG = True

# instantiate the app
app = Flask(__name__)
app.config.from_object(__name__)

# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})

#connect db and create items table
sqlm.connect()

#insert some temp values COMMENT THIS OUT AFTER first time
sqlm.insert_item("1", "FantasticPhone", 500.00, 2, "super duper phone", "photolink1")
sqlm.insert_item("2", "FP2", 500.20, 4, "super duper sfsdf", "photolink2")
sqlm.insert_item("3", "FP3", 510.00, 5, "super crappy phone", "photolink3")
sqlm.insert_item("4", "FP3", 510.00, 5, "super crappy phone", "photolink3")
sqlm.insert_item("5", "F2P3", 510.00, 5, "super crappy p", "photolink13")
sqlm.insert_item("6", "FP23", 510.00, 5, "super crappy thing", "photo1link3")

# sanity check route
@app.route('/ping', methods=['GET'])
def ping_pong():
    return jsonify('pong!')

BOOKS = [
    {
        'title': 'On the Road',
        'author': 'Jack Kerouac',
        'read': True
    },
    {
        'title': 'Harry Potter and the Philosopher\'s Stone',
        'author': 'J. K. Rowling',
        'read': False
    },
    {
        'title': 'Green Eggs and Ham',
        'author': 'Dr. Seuss',
        'read': True
    }
]

ITEMS = [
    {
        'uid': 1,
        'name': 'iPhone',
        'quantity': 5,
        'price': 799.99,
        'description': 'Brand new',
    },
    {
        'uid': 2,
        'name':'MacBook Pro',
        'quantity': 2,
        'price': 1299.99,
        'description': 'Slightly used',
    },
    {
        'uid': 3,
        'name': 'Apple Watch',
        'quantity': 10,
        'price': 499.99,
        'description': 'Brand new',
    }
]

@app.route('/books', methods=['GET', 'POST'])
def all_books():
    response_object = {'status': 'success'}
    if request.method == 'POST':
        post_data = request.get_json()
        BOOKS.append({
            'title': post_data.get('title'),
            'author': post_data.get('author'),
            'read': post_data.get('read')
        })
        response_object['message'] = 'Book added!'
    else:
        response_object['books'] = BOOKS
    return jsonify(response_object)

@app.route('/items', methods=['GET','POST'])
def all_items():
    response_object = {'status': 'success'}
    if request.method == 'POST':
        post_data = request.get_json()
        sqlm.insert_item(
            post_data.get('uid'),
            post_data.get('name'),
            post_data.get('price'),
            post_data.get('quantity'),
            post_data.get('description'),
            post_data.get('photo_text'))
        response_object['message'] = 'Item added!'
    else:
        rows = sqlm.get_all_items()
        response_object['items'] = (sqltojson.make_dicts_item(rows))
        return jsonify(response_object)

@app.route('/getitem/<id>', methods=['GET'])
def one_item(id):
    print(type(id))
    print("getting one item ID: "+id)
    rows = sqlm.find_item(id)
    print(sqltojson.make_dicts_item(rows))
    return jsonify({
    'status':'success',
    'items': (sqltojson.make_dicts_item(rows))
    })

def check_password(hashed_password, user_password):
    return hashed_password == hashlib.md5(user_password.encode()).hexdigest()

def validate(username, password):
    con = sqlite3.connect('server/database.db')
    completion = False
    with con:
                cur = con.cursor()
                cur.execute("SELECT * FROM Users")
                rows = cur.fetchall()
                for row in rows:
                    dbUser = row[0]
                    dbPass = row[1]
                    if dbUser==username:
                        completion=check_password(dbPass, password)
    return completion

@app.route('/', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        completion = validate(username, password)
        if completion ==False:
            error = 'Invalid Credentials. Please try again.'
        else:
            return redirect(url_for('secret'))
    return render_template('Login.vue', error=error)

@app.route('/secret')
def secret():
    return "You have successfully logged in"

if __name__ == "__main__":
    app.run(debug=True)
