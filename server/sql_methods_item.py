import sqlite3
def connect():
    conn=sqlite3.connect("database.db")
    cur=conn.cursor()
    cur.execute("DROP TABLE if EXISTS items")
    cur.execute("CREATE TABLE IF NOT EXISTS items (item_id text PRIMARY KEY, item_name text, item_price float, quantity int, description text, photo text)")
    conn.commit()
    conn.close()

def insert_item(item_id, item_name, item_price, quantity, description, photo):
    conn=sqlite3.connect("database.db")
    cur=conn.cursor()
    cur.execute("INSERT INTO items VALUES (?,?,?,?,?,?)",(item_id, item_name, item_price, quantity, description, photo))
    conn.commit()
    conn.close()

def search(item_name):
    conn=sqlite3.connect("database.db")
    cur=conn.cursor()
    cur.execute("SELECT * FROM items WHERE item_name=?", [item_name])
    rows=cur.fetchall()
    conn.close()
    return rows

def find_item(item_id):
    conn=sqlite3.connect("database.db")
    cur=conn.cursor()
    cur.execute("SELECT * FROM items WHERE item_id=?", [item_id])
    rows=cur.fetchall()
    conn.close()
    return rows

def in_price_range(low_price, high_price):
    conn=sqlite3.connect("database.db")
    cur=conn.cursor()
    cur.execute("SELECT * FROM items WHERE item_price>=? and item_price<=?", (low_price, high_price))
    rows=cur.fetchall()
    conn.close()
    return rows

def delete_item(item_id):
    conn=sqlite3.connect("database.db")
    cur=conn.cursor()
    cur.execute("DELETE FROM items WHERE ​item_id​=?",(item_id))
    conn.commit()
    conn.close()

def update_price(item_id, item_price):
    conn=sqlite3.connect("database.db")
    cur=conn.cursor()
    cur.execute("UPDATE items SET item_price=? WHERE ​item_id​=?",(item_price, item_id))
    conn.commit()
    conn.close()

def update_name(item_id, item_name):
    conn=sqlite3.connect("database.db")
    cur=conn.cursor()
    cur.execute("UPDATE items SET item_name=? WHERE ​item_id​=?",(item_name, item_id))
    conn.commit()
    conn.close()

def update_desc(item_id, description):
    conn=sqlite3.connect("database.db")
    cur=conn.cursor()
    cur.execute("UPDATE items SET description=? WHERE ​item_id​=?",(description, item_id))
    conn.commit()
    conn.close()

def update_qty(item_id, quantity):
    conn=sqlite3.connect("database.db")
    cur=conn.cursor()
    cur.execute("UPDATE items SET item_quantity=? WHERE ​item_id​=?",(quantity, item_id))
    conn.commit()
    conn.close()

def update_photo(item_id, photo):
    conn=sqlite3.connect("database.db")
    cur=conn.cursor()
    cur.execute("UPDATE items SET photo=? WHERE ​item_id​=?",(photo, item_id))
    conn.commit()
    conn.close()

def avg_price():
    conn=sqlite3.connect("database.db")
    cur=conn.cursor()
    cur.execute("SELECT avg(item_price) FROM items")
    rows=cur.fetchall()
    conn.close()
    return rows

def count_different_items():
    conn=sqlite3.connect("database.db")
    cur=conn.cursor()
    cur.execute("SELECT count(item_id) FROM items")
    rows=cur.fetchall()
    conn.close()
    return rows

def get_all_items():
    conn=sqlite3.connect("database.db")
    cur=conn.cursor()
    cur.execute("SELECT * FROM items")
    rows=cur.fetchall()
    conn.close()
    return rows
