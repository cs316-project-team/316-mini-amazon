def make_dicts_item(rows):
    fields = ["item_id", "item_name","item_price", "quantity", "description", "photo_text"]
    data = []
    for row in rows:
        tempdic = {}
        for i in range(6):
            tempdic[fields[i]] = row[i]
        data.append(tempdic)
    return data
