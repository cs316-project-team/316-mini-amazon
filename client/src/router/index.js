import Vue from 'vue';
import Router from 'vue-router';
import Items from '../components/Items.vue';
import Ping from '../components/Ping.vue';
import Home from '../components/Home.vue';
import Product from '../components/Product.vue';
import Login from '../components/Login.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/items',
      name: 'Items',
      component: Items,
    },
    {
      path: '/ping',
      name: 'Ping',
      component: Ping,
    },
    {
      path: '/home',
      name: 'Home',
      component: Home,
    },
    {
      path: '/product:id',
      name: 'Product',
      component: Product,
    },
  ],
});
